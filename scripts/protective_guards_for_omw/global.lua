local types = require("openmw.types")
local storage = require("openmw.storage")
local aux_util = require("openmw_aux.util")
local world = require("openmw.world")
local async = require("openmw.async")
local core = require("openmw.core")
local blAreas = require("scripts/protective_guards_for_omw/blacklistedAreas")
local I = require("openmw.interfaces")
local pg_Settings = storage.globalSection("Settings_ProtectiveGuards_Option")
local ProtectiveGuards_I = I.ProtectiveGuards_eqnx

local intDist = pg_Settings:get("Search Guard Distance Interiors")
local extDist = pg_Settings:get("Search Guard Distance Exteriors")

local registeredGuards = {} -- track guards to be used to pacify guards whenever an aggressor becomes immune

async:newUnsavableSimulationTimer(0, function()
    if I.Pursuit_eqnx then
        I.Settings.updateRendererArgument("Settings_ProtectiveGuards_Option", "Search Guard In Nearby Adjacent Cells", {
            disabled = false
        })
        print("Pursuit and Protective Guards interaction established")
    end
end)

pg_Settings:subscribe(async:callback(function(sect, key)
    if key == "Search Guard Distance Interiors" then
        intDist = pg_Settings:get("Search Guard Distance Interiors")
    end
    if key == "Search Guard Distance Exteriors" then
        extDist = pg_Settings:get("Search Guard Distance Exteriors")
    end
end))

local function showDebug(guard, aggressor, victim)
    if storage.globalSection("Settings_ProtectiveGuards_Debug"):get("Debug") then
        local guardRec = guard.type.record(guard)
        local aggRec = aggressor.type.record(aggressor)

        local message = string.format("%s of %s class from %s attacks %s", guardRec.name, guardRec.class, guard.cell.name, aggRec.name)

        pcall(types.Player.sendMenuEvent, victim, "ProtectiveGuards_message_eqnx", message)
    end
end

local function oldVersionCleanup(actor)
    local oldObjs = types.Actor.inventory(actor):findAll("PG_TrigCrime")
    if #oldObjs > 0 then
        for _, oldObj in pairs(oldObjs) do
            if oldObj:isValid() then
                oldObj:remove()
            end
        end
    end
end

local function getGuardsFromCell(cell)
    return aux_util.mapFilter(cell:getAll(types.NPC), function(actor)
        if ProtectiveGuards_I.isGuard(actor) and not types.Actor.isDead(actor) then
            actor:addScript("scripts/protective_guards_for_omw/protect.lua")
            return true
        else
            if actor:hasScript("scripts/protective_guards_for_omw/protect.lua") then
                actor:removeScript("scripts/protective_guards_for_omw/protect.lua")
                registeredGuards[actor.id] = nil
            end
            return false
        end
    end)
end

local function isImmune(aggressor)
    return ProtectiveGuards_I.immunedAgg[aggressor.recordId] or ProtectiveGuards_I.immunedAgg[aggressor.id]
end

local function alertGuard(guard, aggressor, victim)
    return ProtectiveGuards_I.runHandlers(guard, aggressor, victim)
end

local function searchGuardsAdjacentCells(victim, aggressor)
    local scannedCells = {} -- there can be multiple doors leading to same cell, pick only 1
    local nearbyDoors = victim.cell:getAll(types.Door)

    for _, door in pairs(nearbyDoors) do
        local doorDest = tostring(types.Door.destCell(door))
        local searchDistance = victim.cell.isExterior and extDist or intDist
        local doorToAggDist = (door.position - aggressor.position):length()
        if not scannedCells[doorDest] and door.type.isTeleport(door) and doorToAggDist < searchDistance then
            for _, actor in pairs(getGuardsFromCell(types.Door.destCell(door))) do
                if alertGuard(actor, aggressor, victim) and not isImmune(aggressor) then
                    actor:addScript("scripts/pursuit_for_omw/pursuer.lua")
                    actor:addScript("scripts/pursuit_for_omw/return.lua")
                    actor:sendEvent("Pursuit_chaseCombatTarget_eqnx", {
                        target = aggressor,
                        masa = 0
                    })
                    registeredGuards[actor.id] = actor
                end
            end
            scannedCells[doorDest] = true
        end
    end
end

local function thisActorIsAttacked(data)
    local aggressor, victim = data.aggressor, data.victim

    if not pg_Settings:get("Mod Status") then
        return
    end

    if blAreas[victim.cell.name] then
        return
    end

    if types.NPC.isWerewolf and types.NPC.isWerewolf(victim) then
        return
    end

    for _, guard in pairs(getGuardsFromCell(victim.cell)) do
        local distance = (guard.position - aggressor.position):length()
        local searchDist = victim.cell.isExterior and extDist or intDist
        if isImmune(aggressor) then
            guard:sendEvent("ProtectiveGuards_stopAttack_eqnx", {
                actor = aggressor
            })
        elseif guard ~= victim and distance <= searchDist then
            alertGuard(guard, aggressor, victim)
            registeredGuards[guard.id] = guard
            showDebug(guard, aggressor, victim)
        end
    end

    if pg_Settings:get("Search Guard In Nearby Adjacent Cells") and I.Pursuit_eqnx then
        searchGuardsAdjacentCells(victim, aggressor)
    end
end

return {
    engineHandlers = {
        onSave = function()
            return {
                registeredGuards = registeredGuards
            }
        end,
        onLoad = function(savedData)
            registeredGuards = savedData and savedData.registeredGuards or {}
        end,
        onPlayerAdded = function(player)
            oldVersionCleanup(player)
        end
    },
    eventHandlers = {
        ProtectiveGuards_thisActorIsAttackedBy_eqnx = thisActorIsAttacked
    }
}
