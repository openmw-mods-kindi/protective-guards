local types = require("openmw.types")
local storage = require("openmw.storage")
local aux_util = require("openmw_aux.util")
local pg_Settings = storage.globalSection("Settings_ProtectiveGuards_Option")

local help = [[
    info = Prints mod info
    isActive = Check whether mod is currently active
    checkImmunity(recID) = Check immunity to guards
    addImmunity(recID) = Make obj immune
    removeImmunity(recID) = Remove immunity
    listImmunity = List all immune objects
    isGuard(gameObject) = Check if obj is a guard

    (recID) can be a recordId or Id
    e.g.
    I.ProtectiveGuards_eqnx.addImmunity("fargoth")
]]

local immunedAgg = {}
local handlers = {}
local guardClasses = {} -- future, use class name as key and replace class string from settings with this

return {
    interfaceName = "ProtectiveGuards_eqnx",
    interface = setmetatable({}, {
        __index = function(tbl, key)
            if key == "immunedAgg" then
                return immunedAgg
            end
            if key == "info" then
                return tostring(require("scripts.protective_guards_for_omw.modInfo"))
            end
            if key == "help" then
                return help
            end
            if key == "isActive" then
                return pg_Settings:get("Mod Status")
            end
            if key == "checkImmunity" then
                return function(arg)
                    if type(arg) == "string" then
                        return immunedAgg[arg] or false
                    end
                    return "Argument must be a string"
                end
            end
            if key == "listImmunity" then
                return aux_util.deepToString(immunedAgg)
            end
            if key == "addImmunity" then -- make this npc immune to guard attacks, stops all guard attacks on it
                return function(arg)
                    if type(arg) == "string" then
                        immunedAgg[arg] = true
                        return arg .. " successfully ADDED"
                    end
                    return "Argument must be a string"
                end
            end
            if key == "removeImmunity" then
                return function(arg)
                    if type(arg) == "string" then
                        immunedAgg[arg] = nil
                        return arg .. " successfully REMOVED"
                    end
                    return "Argument must be a string"
                end
            end
            if key == "isGuard" then
                return function(actor)
                    local guardClasses = pg_Settings:get("Search Guard of Class"):lower()
                    local actorClass = actor.type.record(actor).class
                    if actorClass and guardClasses:find(actorClass:lower()) then
                        return true
                    end
                    return false
                end
            end
            if key == "addHandler" then
                return function(fn)
                    handlers[#handlers + 1] = fn
                end
            end
            if key == "runHandlers" then
                return function(guard, aggressor, victim)
                    for i = #handlers, 1, -1 do
                        local bool = handlers[i](guard, aggressor, victim)
                        if bool ~= nil then -- override
                            return bool
                        end
                    end
                    if victim.type == types.Player then -- only players are protected in this mod
                        if types.Player.getCrimeLevel(victim) <= 0 then
                            guard:sendEvent("ProtectiveGuards_alertGuard_eqnx", {
                                aggressor = aggressor
                            })
                            return true
                        end
                    end
                end
            end
        end
    }),
    eventHandlers = {
        ProtectiveGuards_editImmunity_eqnx = function(actorId, isImmune)
            immunedAgg[actorId] = isImmune
        end
    }
}
