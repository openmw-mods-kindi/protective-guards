local ui = require("openmw.ui")
local core = require("openmw.core")
local modInfo = require("scripts.protective_guards_for_omw.modInfo")
local l10n = core.l10n("protective_guards_for_omw")
local I = require("openmw.interfaces")

I.Settings.registerPage {
    key = "PGFOMW_KINDI",
    l10n = "protective_guards_for_omw",
    name = "settings_modName",
    description = l10n("settings_modDesc"):format(modInfo.MOD_VERSION)
}
local function message(message)
    ui.showMessage(tostring(message))
    print(message)
end

return {
    engineHandlers = {
        onFrame = function()
            local str = string.format("[%s] mod requires Lua API %s or newer!", modInfo.MOD_NAME, modInfo.MIN_API)
            assert(core.API_REVISION >= modInfo.MIN_API, str)
        end,
    },
    eventHandlers = {
        ProtectiveGuards_message_eqnx = message
    }
}
