local CHANGES = [[ 
    Code refactoring
    Main logic moved from player script to global script
    Added global interface
    Added quest blacklist
]]

return setmetatable({
    MOD_NAME = "Protective Guards",
    MOD_VERSION = 0.19,
    MIN_API = 60,
    CHANGES = CHANGES
}, {
    __tostring = function(modInfo)
        return string.format("\n[%s]\nVersion: %s\nMinimum API: %s\nChanges: %s", modInfo.MOD_NAME, modInfo.MOD_VERSION, modInfo.MIN_API, modInfo.CHANGES)
    end,
    __metatable = tostring
})

-- require("scripts.protective_guards_for_omw.modInfo")
