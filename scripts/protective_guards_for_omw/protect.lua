local self = require("openmw.self")
local ai = require("openmw.interfaces").AI
local types = require("openmw.types")
local core = require("openmw.core")
local time = require("openmw_aux.time")
local nearby = require("openmw.nearby")

local function stopCombat(actor)
    ai.filterPackages(function(package)
        if package.type == "Combat" then
            return package.target ~= actor
        end
        return true
    end)
end

return {
    engineHandlers = {

    },
    eventHandlers = {
        ProtectiveGuards_stopAttack_eqnx = function(e)
            local T = ai.getActiveTarget("Combat")
            if T and T == e.actor then
                stopCombat(T)
                T:sendEvent("ProtectiveGuards_stopAttackAgg_eqnx", {
                    actor = self.object
                })
            end
        end,
        ProtectiveGuards_alertGuard_eqnx = function(e)
            if not e.aggressor then
                return
            end

            if not types.Actor.canMove(self) or not e.aggressor:isValid() then
                return
            end

            --[[ if types.Actor.isSwimming(e.attacker) then
                local status, path = nearby.findPath(self.position, e.attacker.position, {
                    includeFlags = nearby.NAVIGATOR_FLAGS.Walk + nearby.NAVIGATOR_FLAGS.OpenDoor,
                    agentBounds = types.Actor.getPathfindingAgentBounds(self)

                })
                if status ~= nearby.FIND_PATH_STATUS.Success then
                    return
                end
            end ]]

            ai.startPackage({
                type = "Combat",
                target = e.aggressor,
                cancelOther = false
            })
        end
    }
}
