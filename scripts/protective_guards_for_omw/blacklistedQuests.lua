local core = require("openmw.core")

return {
    engineHandlers = {
        onQuestUpdate = function(id, stage)
            if id == "hr_billcollect" then
                core.sendGlobalEvent("ProtectiveGuards_editImmunity_eqnx", {
                    "giras indaram",
                    stage == 70 and true or nil
                })
            end
            if id == "tr_champion" then
                core.sendGlobalEvent("ProtectiveGuards_editImmunity_eqnx", {
                    "karrod",
                    stage == 70 and true or nil
                })
            end
        end

    }
}
