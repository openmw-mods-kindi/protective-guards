local self = require("openmw.self")
local ai = require("openmw.interfaces").AI
local types = require("openmw.types")
local async = require("openmw.async")
local core = require("openmw.core")

-- todo: guards protect against criminal players
-- note: engine already automatically protect against hostile creatures
-- this script is attached to NPCs only
local function cTScan()
    async:newUnsavableSimulationTimer(math.random() + math.random() * 2, cTScan)
    if types.Actor.isDead(self) then
        return
    end

    local cT = ai.getActiveTarget("Combat")

    if cT and types.NPC.objectIsInstance(cT) then
        core.sendGlobalEvent("ProtectiveGuards_thisActorIsAttackedBy_eqnx", {
            aggressor = self,
            victim = cT,
        })
    end
end

async:newUnsavableSimulationTimer(math.random(), cTScan)

return {
    eventHandlers = {
        ProtectiveGuards_stopAttackAgg_eqnx = function(e)
            ai.filterPackages(function(package)
                if package.type == "Combat" then
                    return package.target ~= e.actor
                end
                --return true
            end)
        end
    }
}
