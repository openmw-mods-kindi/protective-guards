Openmw Lua is still early in development, please consider this. The mod may not be fully feature complete!

Note:
Guards will only protect the player if crime level is 0 or less.
---
If you are playing OpenMW version 0.48, the ESP plugin file is required to obtain crime level (bounty). Without this information, 
this mod will not work properly.
---
If you are playing OpenMW version 0.49 or newer, you can discard the ESP plugin file because crime level can now be obtain through Lua.
---




You are free to modify the mod to suit your personal preference.
More information and installation here https://www.nexusmods.com/morrowind/mods/46992
Do not upload this archive anywhere else without permission.